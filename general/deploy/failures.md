# Deploy Failures

Sometimes a deploy pipeline fails.  We should always be aware of these failures
through notifications coming in through slack on `#announcements`. If the issue cannot be resolved within 5 minutes follow the steps in [Deployment blockers](https://about.gitlab.com/handbook/engineering/releases/#deployment-blockers) to unblock deployments asap.

## Failure Cases

* QA - this would happen after a deployment has completed successfully and now QA
  jobs will execute.  If any of the QA tasks fail, the Quality department is
  notified. The Release Manager should follow up on these failures using the
  [Deployment blockers](https://about.gitlab.com/handbook/engineering/releases/#deployment-blockers)
  process to resolve or mitigate the issue as quickly as possible. Once an
  incident issue is created, release managers should contact the appropriate
  SREs and on-call Quality engineers. A schedule for the Quality on-call
  engineers and more information about this process
  [is found here](https://about.gitlab.com/handbook/engineering/quality/guidelines/#quality-department-pipeline-triage-on-call-rotation).
  * Quality also has helpful guides published here:
    * https://about.gitlab.com/handbook/engineering/quality/guidelines/#test-failures
    * https://about.gitlab.com/handbook/engineering/quality/guidelines/debugging-qa-test-failures/
* Prep Job - The prepare job perform a few items of work, one related to
  ensuring that servers are in the appropriate state in our load balancers and
  that the version of GitLab is the same across the fleet.  Action must be taken
  manually in order to remediate this type of failure.
* Fleet Deploy - This can fail for a myriad of reasons.  Each failure should be
  investigated to determine the appropriate course of action accompanied with an
  issue where appropriate.  Common fixes are noted below.
* Post Deploy Migrations - These are discussed in greater detail below.

## Troubleshooting

### Post-deployment migration failures

Post-deployment migration failures don't appear to deployments, as they take place
after the deployment. Despite this they could be leaving the fleet in an unstable state and as such should be treated as [Deployment blockers](https://about.gitlab.com/handbook/engineering/releases/#deployment-blockers). When creating the issue, don't worry about not having all the details yet; a link to
a failing pipeline with some basic information is enough.

Once the issue is created, determine what the next steps are:

* Do we need SREs to help look into the issue?
* Do we need database experts to help us out?
* Who will take a look at resolving the problem, if this requires any code
  changes?

Make sure to add any additional information to the incident, such as links to
the merge request that introduced the migration. Note that as a release manager
your primary task at this point is to coordinate the effort to resolve the
incident, instead of trying to resolve it yourself. This helps balance the
workload, instead of one person having to do all the work.

If the incident happens at the end of your shift and there is no immediate need
to resolve it, make this clear in both the issue, the appropriate Slack
channels, and inform the next release manager about the state of things. This
ensures the next release manager can take over the work when they begin their
shift.

### Production Deploy Failed, but okay to leave in place

In situations where production deploys fail for any reason (such as post deploy
migration failures), but it is deemed safe to leave production as is, we need to
ensure that we don't prevent future deploys from being blocked.  Run the
following from the `deploy-tooling` repository:

```
CURRENT_DEPLOY_ENVIRONMENT=<env> DEPLOY_VERSION=<version> ./bin/set-version
./bin/set-omnibus-updates gstg --enable
```

This will configure the `<env>-omnibus-version` chef role to the appropriate
version and ensure installation is enabled of that version.  This happens
automatically during successful pipelines.

This is slated to become a chatops run set of commands: https://gitlab.com/gitlab-com/gl-infra/delivery/issues/524

### Prep job discovered nodes in `DRAIN`

The prep job (`<ENV>-prepare`) will fail if nodes are not in state `UP` or `MAINT`. Any other
state and the prep job will hard fail noting which frontend server contains the
state, and which backend server is in this state. Unless there's known
maintenance happening there should not exist a situation where a server isn't in
either of the preferred states.

If a server is in state `DRAIN` a prior deploy may not have fully completed.
In this case, set the server into `MAINT` and retry.

This can be accomplished by following the [documentation for setting haproxy server state](https://gitlab.com/gitlab-com/runbooks/blob/master/howto/haproxy.md#set-server-state).
If you don't have access to the nodes, ask the SRE on call for GitLab Production to help you with this. You can find the engineer on call through ChatOps:

```bash
/chatops run oncall production
```

### Fleet Deploy

* `E: Could not get lock /var/lib/apt/lists/lock - open (11: Resource
  temporarily unavailable)`
  * This is common if there's a collision with chef running and the deploy
    trying to perform similar actions
  * Though we've tried to eliminate these issues as much as possible, hitting
    retry is usually the best method to allow the deploy to continue
* Timeout when setting haproxy state
  * This is common for our `pages` fleet when pages is starting up
  * [Pages service takes a long time to start up], hitting retry for this job
    helps.  The timeout is already very high on this particular task, if we hit
    the timeout again, we should open an issue to investigate further.
  * If this happens on a server unrelated to the `pages` service starting, this
    must be deeply investigated on the node that exhibited the failure

[gitlab issue tracker]: https://gitlab.com/gitlab-org/gitlab/issues
[Pages service takes a long time to start up]: https://gitlab.com/gitlab-org/gitlab-pages/issues/41
